- Projeto submetido como avaliação para o desafio backend da ZX Ventures
https://github.com/ZXVentures/code-challenge/blob/master/backend.md
 A solução foi desenvolvida em python3.6 + django + graphene (GraphQL)



- **Desempenho:**
  - As funções principais do aplicativo são CRUD, logo o desempenho depende principalmente do banco de dados.
  O desafio está em evitar iterar por todos os PDVs registrados ao procurar o PDV mais próximo.
  Para isso utiliza-se um índice espacial (r-tree, nesse caso).
  O índice pode ser implementado in-memory, mas existem bancos de dados que oferecem extensões para dados espaciais.
  Aqui a implementação utilizada foi o módulo rtree do SQLite, para manter a simplicidade de deploy.

- **Cross-Platform**
  - As Python + SQLite garantem Linux e Windows
  O uso no Windows depende da instalação prévia do spatialite e da biblioteca GDAL.


- **Instruções para deploy:**
  - Python 3.6
  - pip install -r requirements.txt
  - python3 manage.py migrate
  - python3 manage.py runserver
  - testes: python3 manage.py test