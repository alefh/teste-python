import json

with open('../zedelivery_api/fixtures/pdvs.json') as f:
    pdvs = json.loads(f.read())
    pdvs_django = [{'model': 'zedelivery_api.PdvModel',
                    'pk': int(pdv["id"]),
                    "fields": {"trading_name": pdv["tradingName"],
                               "owner_name": pdv["ownerName"],
                               "document": pdv["document"],
                               "coverage_area": pdv["coverageArea"],
                               "address": pdv["address"],
                               }
                    } for pdv in pdvs["pdvs"]]

print(json.dumps(pdvs_django, indent='  ',), file=open('../zedelivery_api/fixtures/pdvs_django.json', 'w'), sep='\n')
