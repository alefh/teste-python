import unittest
from django.test import TestCase
from graphene.test import Client
import snapshottest.django
from zedelivery_api.models import PdvModel, AddressIndexTreeModel, CoverageTreeModel
from zedelivery_api.spatial import get_bounding_box, nearest_intersection
from zedelivery_api.schema import schema


class RelatedIndicesTestCase(TestCase):
    pdv = None

    def setUp(self):
        pdv_fixture = {"pk": 1,
                       "trading_name": "Adega Osasco",
                       "owner_name": "Ze da Ambev",
                       "document": "02.453.716/000170",
                       "address": {"type": "Point",
                                   "coordinates": [0.5, 0.6]},
                       "coverage_area": {
                           "type": "MultiPolygon",
                           "coordinates": [[[[0, 0],
                                             [1, 1],
                                             [0.5, 0.5],
                                             [1.5, 1.5],
                                             [0, 0]
                                             ]]]}}
        self.pdv = PdvModel.objects.create(**pdv_fixture)

    def test_address_created(self):
        addr = AddressIndexTreeModel.objects.get(pk=self.pdv.id)
        lng, lat = self.pdv.address['coordinates']
        self.assertAlmostEqual(addr.lng, lng)
        self.assertAlmostEqual(addr.lat, lat)

    def test_coverage_index_created(self):
        bbox = CoverageTreeModel.objects.get(pk=self.pdv.id)
        self.assertAlmostEqual(bbox.ymin, 0)
        self.assertAlmostEqual(bbox.xmax, 1.5)
        self.assertAlmostEqual(bbox.xmin, 0)
        self.assertAlmostEqual(bbox.ymax, 1.5)


class SpatialTests(TestCase):
    def test_bbox(self):
        coords = [[[[-44, -23], [-45, -22], [-44.5, -22.5], [-43.5, -22.5], [-44, -23]]]]
        bbox = get_bounding_box(coords)
        self.assertEqual(bbox, {"xmin": -45.0, "xmax": -43.5, "ymin": -23.0, "ymax": -22.0})


class ApiTests(TestCase):
    fixtures = ['pdvs_django']

    def test_pdv_by_id(self):
        client = Client(schema)
        pdv = client.execute('{  pdvById(id: 1) {id}}')
        expected = {'data': {'pdvById': {'id': '1'}}}
        self.assertDictEqual(pdv, expected)

    def test_nearest_pdv(self):
        """pdv mais próximo do próprio endereço deve ser sempre ele mesmo"""
        pdvs = PdvModel.objects.exclude(pk__in=(14, 17, 47, 48))  # esses endereços estão mal-posiciondos
        for pdv in pdvs:
            mais_proximo = nearest_intersection(*pdv.address['coordinates'])
            self.assertEqual(mais_proximo.pk, pdv.pk)

    def test_no_near_pdv(self):
        mais_proximo = nearest_intersection(78, 78)
        self.assertIsNone(mais_proximo)


class APITestCase(snapshottest.django.TestCase):
    fixtures = ['pdvs_django']

    def test_api_pdv_by_id(self):
        """Testing the API for /pdvByid"""
        client = Client(schema)
        self.assertMatchSnapshot(client.execute('''{ pdvById }'''))

    def test_create_pdv(self):
        client = Client(schema)
        created = client.execute(
            """
    mutation {
    createPdv(tradingName: "Adega Osasco",
                        ownerName: "Ze da Ambev",
                        document: "02.453.716/000170",
                        coverageArea: {type: "MultiPolygon",
                        coordinates: [[[[-43.36556, -22.99669], 
                                        [-43.36539, -23.01928],
                                        [-43.25724,-23.00649],
                                        [-43.23355,-23.00127],
                                        [-43.36556, -22.99669],]]]},
                        address: {type: "Point", coordinates: [-43.297337, -23.013538]}) 
    {
    pdv {
      address
      coverageArea
      ownerName
      tradingName
      document
    }
  }
}""")
        self.assertMatchSnapshot(created)

    def test_nearest_pdv(self):
        client = Client(schema)
        nearest = client.execute("""query
{
  nearestPdv(point: {coordinates: [-43.297340393066406, -23.013534545898438]}) {
    id
    coverageArea
  }
}""")
        self.assertMatchSnapshot(nearest)


if __name__ == '__main__':
    unittest.main()
