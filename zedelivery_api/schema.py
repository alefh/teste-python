from graphene_django import DjangoObjectType
import graphene
import re
from zedelivery_api.models import PdvModel
from zedelivery_api.spatial import nearest_intersection


class Pdv(DjangoObjectType):
    pk = graphene.ID()

    class Meta:
        model = PdvModel


class MultiPolygonInput(graphene.InputObjectType):
    type = graphene.String()
    coordinates = graphene.List(graphene.List(graphene.List(graphene.List(graphene.Float, required=True))))


class AddressInput(graphene.InputObjectType):
    type = graphene.String()
    coordinates = graphene.List(graphene.Float, required=True)


class Query(graphene.ObjectType):
    pdv_by_id = graphene.Field(Pdv, id=graphene.ID())
    nearest_pdv = graphene.Field(Pdv, point=AddressInput(required=True))

    def resolve_pdv_by_id(self, info, id):
        return PdvModel.objects.get(pk=id)

    def resolve_nearest_pdv(self, info, point):
        return nearest_intersection(*point.coordinates)


class CreatePdv(graphene.Mutation):
    class Arguments:
        tradingName = graphene.String(required=True)
        ownerName = graphene.String(required=True)
        document = graphene.String(required=True)
        address = AddressInput(required=True)
        coverageArea = MultiPolygonInput(required=True)

    pdv = graphene.Field(Pdv)

    def mutate(self, info, **kwargs):
        pdv = PdvModel(
            trading_name=kwargs['tradingName'],
            owner_name=kwargs['ownerName'],
            document=re.sub('[\D]', '', kwargs['document']),
            address=kwargs['address'],
            coverage_area=kwargs['coverageArea']
        )
        pdv.save()
        return CreatePdv(pdv=pdv)


class Mutation(graphene.ObjectType):
    create_pdv = CreatePdv.Field()


schema = graphene.Schema(query=Query, mutation=Mutation)
