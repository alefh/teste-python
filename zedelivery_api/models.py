from django.db import models
from djgeojson.fields import PointField, MultiPolygonField
import pycpfcnpj.cnpj
from zedelivery_api.spatial_utils import coords_to_multipolygon


def multi_polygon_validator(field):
    coords = field['coordinates']
    mp = coords_to_multipolygon(coords)
    return mp.valid


class PdvModel(models.Model):
    trading_name = models.CharField(max_length=100, null=False)
    owner_name = models.CharField(max_length=100, null=False)
    document = models.CharField(max_length=14, null=False, unique=True, validators=[pycpfcnpj.cnpj.validate])
    address = PointField()
    coverage_area = MultiPolygonField(validators=[multi_polygon_validator])


class CoverageTreeModel(models.Model):
    Pdv = models.OneToOneField(primary_key=True, to=PdvModel, on_delete=models.CASCADE, db_column='pdv_id')
    xmin = models.FloatField(null=False)
    xmax = models.FloatField(null=False)
    ymin = models.FloatField(null=False)
    ymax = models.FloatField(null=False)

    class Meta:
        managed = False
        db_table = 'coverage_tree'


class AddressIndexTreeModel(models.Model):
    Pdv = models.OneToOneField(primary_key=True, to=PdvModel, on_delete=models.CASCADE, db_column='pdv_id')
    lng = models.FloatField(null=False)
    lat = models.FloatField(null=False)

    class Meta:
        managed = False
        db_table = 'address_index'
