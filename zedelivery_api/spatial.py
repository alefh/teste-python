from django.db.models import signals
from django.dispatch import receiver
from django.db.models import F
from django.contrib.gis.geos import Point

from zedelivery_api.models import PdvModel, CoverageTreeModel, AddressIndexTreeModel
from zedelivery_api.spatial_utils import coords_to_multipolygon


def get_bounding_box(coords):
    multi_polygon = coords_to_multipolygon(coords)
    xmin, ymin, xmax, ymax = multi_polygon.extent
    return {"xmin": xmin, "xmax": xmax, "ymin": ymin, "ymax": ymax}


@receiver(signals.post_save, sender=PdvModel)
def update_rtree_index(sender, instance, created, raw, **kwargs):
    """atualiza os índices espaciais no banco de dados a cada alteração.
    Calcula uma bounding-box da área de cobertura como aproximação."""
    if raw or isinstance(instance.coverage_area, dict):  # lidar com manage.py loaddata e com criação nos testes
        parameter = instance.coverage_area['coordinates']
        address = instance.address['coordinates']
    else:
        parameter = instance.coverage_area.coordinates
        address = instance.address.coordinates
    bbox = get_bounding_box(parameter)
    CoverageTreeModel.objects.update_or_create(pk=instance.id, **bbox)
    AddressIndexTreeModel.objects.update_or_create(pk=instance.id, lng=address[0], lat=address[1])


def check_intersection(pdv, lng, lat):
    """checa se a coverage_area do pdv atende esse ponto"""
    p = Point(lng, lat)
    mp = coords_to_multipolygon(pdv.coverage_area['coordinates'])
    return p.intersects(mp)


def intersection_candidates(lat, lng):
    """encontra todos os pdvs cuja bounding-box tenha interseção com um ponto"""
    inside_bbox = CoverageTreeModel.objects.filter(xmin__lte=lng,  # pdv dentro da bounding-box?
                                                   xmax__gte=lng,
                                                   ymin__lte=lat,
                                                   ymax__gte=lat).values_list('Pdv_id', flat=True)
    distancia = (F('lng') - lng) * (F('lng') - lng) + \
                (F('lat') - lat) * (F('lat') - lat)  # sem sqrt, só preciso da grandeza
    return AddressIndexTreeModel.objects.order_by(
        distancia).filter(pk__in=inside_bbox).select_related('Pdv')


def nearest_intersection(lng, lat):
    """retorna o pdv mais próximo que atenda o ponto solicitado"""
    for c in intersection_candidates(lat, lng):
        if check_intersection(c.Pdv, lng, lat):
            return c.Pdv
