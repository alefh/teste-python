from django.contrib.gis.geos import Polygon, MultiPolygon


def coords_to_multipolygon(coords):
    polygons = [Polygon(*poly) for poly in coords]
    mp = MultiPolygon(polygons)
    return mp
