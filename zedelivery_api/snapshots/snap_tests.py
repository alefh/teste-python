# -*- coding: utf-8 -*-
# snapshottest: v1 - https://goo.gl/zC4yUc
from __future__ import unicode_literals

from snapshottest import Snapshot


snapshots = Snapshot()

snapshots['APITestCase::test_api_pdv_by_id 1'] = {
    'errors': [
        {
            'locations': [
                {
                    'column': 3,
                    'line': 1
                }
            ],
            'message': 'Field "pdvById" of type "Pdv" must have a sub selection.'
        }
    ]
}

snapshots['APITestCase::test_nearest_pdv 1'] = {
    'data': {
        'nearestPdv': {
            'coverageArea': "{'type': 'MultiPolygon', 'coordinates': [[[[-43.36556, -22.99669], [-43.36539, -23.01928], [-43.26583, -23.01802], [-43.25724, -23.00649], [-43.23355, -23.00127], [-43.2381, -22.99716], [-43.23866, -22.99649], [-43.24063, -22.99756], [-43.24634, -22.99736], [-43.24677, -22.99606], [-43.24067, -22.99381], [-43.24886, -22.99121], [-43.25617, -22.99456], [-43.25625, -22.99203], [-43.25346, -22.99065], [-43.29599, -22.98283], [-43.3262, -22.96481], [-43.33427, -22.96402], [-43.33616, -22.96829], [-43.342, -22.98157], [-43.34817, -22.97967], [-43.35142, -22.98062], [-43.3573, -22.98084], [-43.36522, -22.98032], [-43.36696, -22.98422], [-43.36717, -22.98855], [-43.36636, -22.99351], [-43.36556, -22.99669]]]]}",
            'id': '1'
        }
    }
}

snapshots['APITestCase::test_create_pdv 1'] = {
    'data': {
        'createPdv': {
            'pdv': {
                'address': "{'type': 'Point', 'coordinates': [-43.297337, -23.013538]}",
                'coverageArea': "{'type': 'MultiPolygon', 'coordinates': [[[[-43.36556, -22.99669], [-43.36539, -23.01928], [-43.25724, -23.00649], [-43.23355, -23.00127], [-43.36556, -22.99669]]]]}",
                'document': '02453716000170',
                'ownerName': 'Ze da Ambev',
                'tradingName': 'Adega Osasco'
            }
        }
    }
}
